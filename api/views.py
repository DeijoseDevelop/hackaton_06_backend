from .models import *
from .serializers import *
from django.http import Http404
from django.contrib.auth import authenticate, login, logout
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework.viewsets import ModelViewSet
#from rest_framework import permissions
#from django.shortcuts import render
#from django.http import Http404
#from utils.random_number import random_hundred, random_countries
#from utils.born_date import born_date
#import time
#from utils.validate_notes import validate_notes


class LoginView(APIView):
    """
    # "If the user is authenticated, log them in and return their data, otherwise return a 404."

    # The first thing we do is get the username and password from the request.data. We then use Django's
    # authenticate method to check if the user is valid. If the user is valid, we log them in using
    # Django's login method and return their data. If the user is not valid, we return a 404
    """
    def post(self, request):
        """
        If the user is authenticated, log them in and return their data, otherwise return a 404

        :param request: The request object is a standard Django request object
        :return: The user object is being returned.
        """
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return Response(
                UserSerializer(user).data,
                status=status.HTTP_200_OK
            )
        return Response(status=status.HTTP_404_NOT_FOUND)

class LogoutView(APIView):
    def post(self, request):
        """
        It logs out the user and returns a 200 OK response

        :param request: The request object
        :return: The status code 200 is being returned.
        """
        logout(request)
        return Response(status=status.HTTP_200_OK)


class SignupView(generics.CreateAPIView):
    serializer_class = UserSerializer


class EventsViewSet(ModelViewSet):
    queryset = Events.objects.all()
    serializer_class = EventsSerializer


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data,many =isinstance(request.data,list))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data,status=status.HTTP_201_CREATED,headers=headers)




