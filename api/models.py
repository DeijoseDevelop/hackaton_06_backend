from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    pass


class Events(models.Model):
    url= models.CharField(max_length=255)
    description = models.CharField(max_length=100)